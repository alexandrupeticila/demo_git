import java.sql.*;

public class Main {

    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://localhost:3306/curs9_nov";
        /*Connection connection = DriverManager.getConnection(url, "root", "");

        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);*/

        /*String insertStatement = "INSERT INTO utilizatori VALUES (null ,'Alex',25)";
        statement.executeUpdate(insertStatement);*/
        /*String deleteSQLStatement = "DELETE FROM utilizatori WHERE nume = 'Alexandru'";
        statement.executeUpdate(deleteSQLStatement);*/

        try (Connection connection = DriverManager.getConnection(url, "root", "");
             Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);) {

            String selectSQLStatement = "SELECT * FROM utilizatori";
            ResultSet resultSet = statement.executeQuery(selectSQLStatement);

            int linie = 0;
            while (resultSet.next()) {
                linie++;
                if (linie == 2) {
                    resultSet.previous();
                }
                int id = resultSet.getInt("id");
                String name = resultSet.getString("nume");
                int varsta = resultSet.getInt("varsta");

                // Statement statement2;
                // resultSet2 = statement2.executeQuery(altSelectQuery)
                System.out.println(id + " " + name + " " + varsta);
            }
        }


    }
}
